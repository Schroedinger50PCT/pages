;; -*- lexical-binding: t -*-

;; dont forget to pull in the changes in the dots submodule before publishing
;; git submodule update --recursive --remote

;; (require 'package)
;; (setq package-user-dir (expand-file-name "./.packages"))
;; (setq package-archives '(("melpa" . "https://melpa.org/packages/")
;;                          ("elpa" . "https://elpa.gnu.org/packages/")))

;; (package-initialize)
;; (unless package-archive-contents
;;   (package-refresh-contents))

(require 'ox-publish)

(setq org-publish-project-alist
      `(("blog.suschem.ist" :components ("index" "imprint" "webroot" "blog" "media" "resume" "dots" "export" "gpoints" "gpoints-scripts"))
	("index"
         ;;:recursive t
         :base-directory "./"
	 :exclude ".*"
         :include ("index.org")
         :publishing-function org-html-publish-to-html
         :publishing-directory "./"
	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-scripts nil
	 :html-head-include-default-style nil
	 :html-head "<link rel=\"stylesheet\" href=\"modus-vivendi.css\" type=\"text/css\" />
<!-- fallback/default icons -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\">

<!-- light mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_black.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: light)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">

<!-- Dark mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\"media=\"(prefers-color-scheme: dark)\">
"
	 :html-preamble "<top>
<a href=\"imprint.html\">Impressum</a> | 
<a href=\"contact.html\">Kontakt</a> | 
<a href=\"cv_jakob_maximilian_honal.html\">CV</a> | 
<a href=\"https://codeberg.org/Schroedinger50PCT\">Git</a>
</top>"
	 :html-postamble "<hr/> <footer>
<div class=\"generated\"> Created with %c on <a href=\"https://guix.gnu.org\">GUIX</a></div>
<div class=\"copyright-container\"> <div class=\"copyright\"> Copyright &copy; 2024-2027 Jakob Maximilian Honal some rights reserved.
<br/> Content is available under <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> CC-BY-SA 4.0 </a> and <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> GPL-3.0-or-later </a> unless otherwise noted. 
 <div class=\"license-badges\"> <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> <img alt=\"Creative Commons License\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /> </a>  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> <img alt=\"Gnu Public License\" src=\"https://www.gnu.org/graphics/gplv3-with-text-136x68.png\" /> </a> </div> </div>
 </footer>"

         :with-author nil          
         :with-creator nil         
         :with-toc t            
         :section-numbers nil      
	 :htmlized-source t
         :time-stamp-file nil
	 )
	("imprint"
         ;;:recursive t
         :base-directory "./"
	 :exclude ".*"
         :include ("imprint.org")
         :publishing-function org-html-publish-to-html
         :publishing-directory "./"
	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-scripts nil
	 :html-head-include-default-style nil
	 :html-head "<link rel=\"stylesheet\" href=\"modus-vivendi.css\" type=\"text/css\" />
<!-- fallback/default icons -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\">

<!-- light mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_black.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: light)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">

<!-- Dark mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\"media=\"(prefers-color-scheme: dark)\">
"
	 :html-preamble ""
	 
	 :html-postamble "<hr/> <footer>
<div class=\"generated\"> Created with %c on <a href=\"https://guix.gnu.org\">GUIX</a></div>
<div class=\"copyright-container\"> <div class=\"copyright\"> Copyright &copy; 2024-2027 Jakob Maximilian Honal some rights reserved.
<br/> Content is available under <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> CC-BY-SA 4.0 </a> and <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> GPL-3.0-or-later </a> unless otherwise noted. 
 <div class=\"license-badges\"> <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> <img alt=\"Creative Commons License\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /> </a>  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> <img alt=\"Gnu Public License\" src=\"https://www.gnu.org/graphics/gplv3-with-text-136x68.png\" /> </a> </div> </div>
 </footer>"

         :with-author nil          
         :with-creator nil         
         :with-toc t            
         :section-numbers nil      
	 :htmlized-source t
         :time-stamp-file nil
	 )
	
	("webroot"
         ;;:recursive t
         :base-directory "./"
	 :exclude "dots\\|index.org\\|imprint.org"
         :publishing-function org-html-publish-to-html
         :publishing-directory "./"
	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-scripts nil
	 :html-head-include-default-style nil
	 :html-head "<link rel=\"stylesheet\" href=\"modus-vivendi.css\" type=\"text/css\" />
<!-- fallback/default icons -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\">

<!-- light mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_black.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: light)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">

<!-- Dark mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\"media=\"(prefers-color-scheme: dark)\">
"
	 :html-preamble "<top> <a href=\"index.html\">Home</a> | <a href=\"imprint.html\">Impressum</a> | <a href=\"contact.html\">Kontakt</a> | <a href=\"cv_jakob_maximilian_honal.html\">CV</a> | <a href=\"https://codeberg.org/Schroedinger50PCT\">Git</a> </top>"
	 :html-postamble "<hr/> <footer>
<div class=\"generated\"> Created with %c on <a href=\"https://guix.gnu.org\">GUIX</a></div>
<div class=\"copyright-container\"> <div class=\"copyright\"> Copyright &copy; 2024-2027 Jakob Maximilian Honal some rights reserved.
<br/> Content is available under <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> CC-BY-SA 4.0 </a> and <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> GPL-3.0-or-later </a> unless otherwise noted. 
 <div class=\"license-badges\"> <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> <img alt=\"Creative Commons License\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /> </a>  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> <img alt=\"Gnu Public License\" src=\"https://www.gnu.org/graphics/gplv3-with-text-136x68.png\" /> </a> </div> </div>
 </footer>"

         :with-author nil          
         :with-creator nil         
         :with-toc t            
         :section-numbers nil      
	 :htmlized-source t
         :time-stamp-file nil
	 
	 )
	("blog"
         :recursive t
         :base-directory "./blog"
         :publishing-function org-html-publish-to-html
         :publishing-directory "./"
	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-scripts nil
	 :html-head-include-default-style nil
	 :html-head "<link rel=\"stylesheet\" href=\"modus-vivendi.css\" type=\"text/css\" />
<!-- fallback/default icons -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\">

<!-- light mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_black.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: light)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">

<!-- Dark mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\"media=\"(prefers-color-scheme: dark)\">
"
	 
	 :html-preamble "<top>
<a href=\"index.html\">Home</a> |
 <a href=\"imprint.html\">Impressum</a> |
 <a href=\"contact.html\">Kontakt</a> |
 <a href=\"cv_jakob_maximilian_honal.html\">CV</a> |
 <a href=\"https://codeberg.org/Schroedinger50PCT\">Git</a>
</top>"
	 :html-postamble "<hr/> <footer>
<div class=\"generated\"> Created with %c on <a href=\"https://guix.gnu.org\">GUIX</a></div>
<div class=\"copyright-container\"> <div class=\"copyright\"> Copyright &copy; 2024-2027 Jakob Maximilian Honal some rights reserved.
<br/> Content is available under <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> CC-BY-SA 4.0 </a> and <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> GPL-3.0-or-later </a> unless otherwise noted. 
 <div class=\"license-badges\"> <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> <img alt=\"Creative Commons License\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /> </a>  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> <img alt=\"Gnu Public License\" src=\"https://www.gnu.org/graphics/gplv3-with-text-136x68.png\" /> </a> </div> </div>
 </footer>"

         :with-author nil          
         :with-creator nil         
         :with-toc t            
         :section-numbers nil      
	 :htmlized-source t
         :time-stamp-file nil
	 )

	 ("resume"
	  :base-directory "./"
	  :exclude ".*"
          :include ("README.org")
	  :base-extension "org"
	  :publishing-directory "./"
	  :publishing-function org-latex-publish-to-pdf)
	 
	("media"
         :base-directory "./media"
         :base-extension "css\\|txt\\|jpg\\|jpeg\\|gif\\|png\\|webp\\|webm\\|opus\\|ogg\\|mp4\\|mp3\\|vorbis\\|svg\\|bmp\\|flac\\|aac\\|wav"
         :recursive t
         :publishing-directory  "./media"
         :publishing-function org-publish-attachment
	 :time-stamp-file nil
	 :auto-sitemap nil
	 ;; :sitemap-title "Medien"
	 ;; :sitemap-filename "media.org"
	 )
	
	("dots"
         ;;:recursive t
         :base-directory "./dots"
         :publishing-function org-html-publish-to-html
         :publishing-directory "./"
	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-scripts nil
	 :html-head-include-default-style nil
	 :html-head "<link rel=\"stylesheet\" href=\"modus-vivendi.css\" type=\"text/css\" />
<!-- fallback/default icons -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\">

<!-- light mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_black.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: light)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_black.png\" media=\"(prefers-color-scheme: light)\">

<!-- Dark mode -->
<link rel=\"icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" type=\"image/png\" href=\"media/suschem_logo_favicon_white.png\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"shortcut icon\" href=\"media/suschem_logo_favicon_white.png\" sizes=\"180x180\" media=\"(prefers-color-scheme: dark)\">
<link rel=\"apple-touch-icon\" href=\"media/suschem_logo_favicon_white.png\"media=\"(prefers-color-scheme: dark)\">
"
	 
	 :html-preamble "<top> <a href=\"index.html\">Home</a> | <a href=\"imprint.html\">Impressum</a> | <a href=\"contact.html\">Kontakt</a> | <a href=\"cv_jakob_maximilian_honal.html\">CV</a> | <a href=\"https://codeberg.org/Schroedinger50PCT\">Git</a> </top>"
	 :html-postamble "<hr/> <footer>
<div class=\"generated\"> Created with %c on <a href=\"https://guix.gnu.org\">GUIX</a></div>
<div class=\"copyright-container\"> <div class=\"copyright\"> Copyright &copy; 2024-2027 Jakob Maximilian Honal some rights reserved.
<br/> Content is available under <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> CC-BY-SA 4.0 </a> and <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> GPL-3.0-or-later </a> unless otherwise noted. 
 <div class=\"license-badges\"> <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> <img alt=\"Creative Commons License\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /> </a>  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> <img alt=\"Gnu Public License\" src=\"https://www.gnu.org/graphics/gplv3-with-text-136x68.png\" /> </a> </div> </div>
 </footer>"

         :with-author nil          
         :with-creator nil         
         :with-toc t            
         :section-numbers nil      
	 :htmlized-source t
         :time-stamp-file nil
	 )
	
	("export"
         :base-directory "./dots/export"
         :base-extension "css\\|txt\\|jpg\\|jpeg\\|gif\\|png\\|webp\\|webm\\|opus\\|ogg\\|mp4\\|mp3\\|vorbis\\|svg\\|bmp\\|flac\\|aac\\|wav"
         :recursive t
	 :exclude ".*"
         :include ("modus-vivendi.css")
         :publishing-directory  "./"
         :publishing-function org-publish-attachment
	 :time-stamp-file nil
	 :auto-sitemap nil
	 )
	
	("gpoints-scripts"
         :base-directory "./gpoints/"
         :base-extension "css\\|js\\|json\\|jpg\\|jpeg\\|gif\\|png\\|webp\\|webm\\|opus\\|ogg\\|mp4\\|mp3\\|vorbis\\|svg\\|bmp\\|flac\\|aac\\|wav"
         :recursive t
         :publishing-directory  "./"
         :publishing-function org-publish-attachment
	 :time-stamp-file nil
	 :auto-sitemap nil
	 )

	("gpoints"
	 :recursive t
         :base-directory "./gpoints/"
	 :exclude nil
         :publishing-function org-html-publish-to-html
         :publishing-directory "./"
	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-scripts nil
	 :html-head-include-default-style nil
	 :html-head "<link rel=\"stylesheet\" href=\"gpoints.css\" type=\"text/css\" />
<link rel=\"manifest\" href=\"/manifest.json\">

<!-- fallback/default icons -->
<link rel=\"icon\" type=\"image/png\" href=\"division3zehen_white-512.png\">
<link rel=\"mask-icon\" type=\"image/png\" href=\"ddivision3zehen_white-512-maskable.png\">
<link type=\"image/png\" href=\"ddivision3zehen_white-512.png\" rel=\"apple-touch-startup-image\">

<meta name=\"description\" content=\"Das Punkte-System, bei dem das Sammeln wirklich Spaß macht. Jetzt Punkte berechnen!\">
<meta name=\"keywords\" content=\"G-Points, Punkte, G-Punkte, points, gpunkte, gpoints\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
<meta name=\"viewport\" content=\"uc-fitscreen=yes\"/>
<meta name=\"layoutmode\" content=\"fitscreen/standard\">
<meta name=\"imagemode\" content=\"force\">
<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\" />
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"default\">
<meta name=\"application-name\" content=\"G-Punkte Rechner\">
<meta name=\"apple-mobile-web-app-title\" content=\"G-Punkte Rechner\">
<meta name=\"theme-color\" content=\"#000000\">
<meta name=\"mobile-web-app-capable\" content=\"yes\">

<meta name=\"msapplication-tooltip\" content=\"Das Punkte-System, bei dem das Sammeln wirklich Spaß macht. Jetzt Punkte berechnen!\">
<meta name=\"msapplication-starturl\" content=\"/\">
<meta name=\"msapplication-TileImage\" content=\"division3zehen_white-512.png\">
<meta name=\"msapplication-TileColor\" content=\"#000000\">
<meta name=\"msapplication-navbutton-color\" content=\"#000000\">
<meta name=\"msapplication-tap-highlight\" content=\"no\">
<meta name=\"full-screen\" content=\"yes\">
<meta name=\"browsermode\" content=\"application\">


"
	 
	 :html-preamble ""
	 
	 :html-postamble "<hr/>
<footer>
<div class=\"footer_links\">
<a href=\"https://blog.suschem.ist/imprint.html\">Impressum</a> | 
<a href=\"https://codeberg.org/Schroedinger50PCT/gpoints\">Git</a>
</div>
<div class=\"generated\"> Created with %c on <a href=\"https://guix.gnu.org\">GUIX</a></div>
<div class=\"copyright-container\">
<div class=\"copyright\"> Copyright &copy; 2024-2027 Jakob Maximilian Honal some rights reserved.
<br/> Content is available under <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> CC-BY-SA 4.0 </a> and <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> GPL-3.0-or-later </a> unless otherwise noted. 
 <div class=\"license-badges\"> <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"> <img alt=\"Creative Commons License\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /> </a>  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0-standalone.html\"> <img alt=\"Gnu Public License\" src=\"https://www.gnu.org/graphics/gplv3-with-text-136x68.png\" /> </a> </div> </div>
 </footer>"

         :with-author nil          
         :with-creator nil         
         :with-toc t            
         :section-numbers nil      
	 :htmlized-source t
         :time-stamp-file nil
	 )
	
	))
