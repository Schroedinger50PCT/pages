//
//  register PWA service worker

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js')
    .then(() => console.log('Service Worker registered successfully.'))
    .catch(error => console.error('Service Worker registration failed:', error));
}


// The following code was transpiled from mechanics.scm
//
// Eingabevariablen (int beginnend bei 0):
// PartnerIn ist aus... -> aus
// PartnerIn hat Amt... -> amt
// PartnerIn ist Mitglied im... -> mit
// Ist PartnerIn PriesterIn? -> priest
// Wieviele Klötzchen hat PartnerIn? -> holz


function calculatePoints() {
    
    const aus = +document.getElementById("select_aus").value;
    const amt = +document.getElementById("select_amt").value;
    const mit = +document.getElementById("select_mit").value;
    const priest = +document.getElementById("select_priest").value;
    const holz = +document.getElementById("select_holz").value;

    const alist = [1, 2, 4, 6, 8, 12, 18, 24, 32, 40, 48, 64, 56];
    const blist = [8, 16, 16, 24, 32, 32, 48, 64, 64, 80, 96, 128, 112];
    const clist = [12, 24, 24, 36, 48, 48, 72, 96, 96, 120, 144, 192, 168];

    let punkte = 0;

    if (amt === 0) {
	// Do nothing
    } else if (aus === 1) {
	punkte = alist[amt - 1];
    } else if (aus === 2) {
	punkte = [2, 4, ...alist.slice(2)][amt - 1];
    } else if (aus === 3) {
	punkte = [3, 6, 6, 9, 12, ...alist.slice(4)][amt - 1];
    } else if (aus === 4) {
	punkte = [4, 8, 8, 12, 16, 16, 24, 32, 3, ...alist.slice(8)][amt - 1];
    } else if (aus === 5) {
	punkte = blist[amt - 1];
    } else if (aus === 6) {
	punkte = clist[amt - 1];
    }

    if (amt > 0) {
	punkte = (Math.pow(2, priest) * (parseFloat(punkte) + [0, 1, 2, 3, 5][mit])) + (holz * 0.5);
    }
    
    document.getElementById("score").textContent = punkte;   
}


document.getElementById("calculate").addEventListener("click", calculatePoints);

const selects = document.querySelectorAll("select");
selects.forEach((select) => {
  select.addEventListener("change", calculatePoints);
});
