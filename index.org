#+title: suschem.ist -- Blog
#+options: toc:nil
#+language: de
#+latex_header: \usepackage[AUTO]{babel}

@@html:<a class="gototop" href="#">↑</a>@@

#+attr_html: :width 50% loading=lazy decoding=async
[[./media/suschem_logo.png]]
* Nominatio
# https://github.com/fniessen/org-macros
#+MACRO: color @@html:<span style="color: $1">$2</span>@@@@latex:\textcolor{$1}{$2}@@@@odt:<text:span text:style-name="$1">$2</text:span>@@
 Der Name dieser Seite {{{color(limegreen,sus)}}} {{{color(magenta,chem)}}}  ist kurz für {{{color(limegreen,sustainable)}}} {{{color(magenta, chemistry)}}}  also "nachhaltige Chemie". \\
 Gleichzeitig ist es eine Anspielung auf "sussy" (aber leider durchaus geläufige) Praktiken wie Greenwashing,
 Whitewashing oder restriktionsarmes Handeln mit dual-use Stoffen. \\
 Diese stehen im direkten Wiederspruch zu ökologischer und gesellschaftlicher Nachhaltigkeit und sind daher besonders scharf zu kritisieren.
* Microblogartikel
** [[file:dots.html][Dot-Conf-Files]]
Hier wachsen meine Dotfiles zu einer deklarativen Konfiguration in Org-Mode heran.

** [[file:mengenstoffe.org][Einblick in die kosmetische Chemie]]
Das ist eine Sammlung von Notizen die ich nach und nach (vor allem mit Verknüpfungen und Belegen) ausbauen will. \\
Bisher ist das wie alles hier noch in Arbeit und man muss selbst Recherche betreiben wenn man konkrete Informationen wünscht:
- Die [[file:mengenstoffe.org][Mengenstoffe]] machen den größten Volumenanteil eines Produktes aus.
- Die Wirkstoffklasse der [[file:tenside.org][Tenside]] ist von derart zentraler Rolle in der Kosmetischen Chemie das sie einen eigenen Aufzählungspunkt verdient.
- Abschließend wird auf weitere wichtige [[file:wirkstoffe.org][Wirkstoffe]] sowie Haltbarkeit und Lagerung von Kosmetika eingegangen.
** [[file:proles.org][Unser Server]]
Hier gibt es weitere Informationen zum guix-proles-ml350g9 Server.
** [[file:gpoints.html][G-Punkte-Rechner]]
Das Punkte-System, bei dem das Sammeln wirklich Spaß macht. \\
Jetzt Punkte berechnen! (nutzt JavaScript)
* Über mich
+ Hier findet sich mein [[file:cv_jakob_maximilian_honal.html][Resume]]
+ Aliasnamen:
  - Schroedinger50PCT
  - SchroedingerPCT50
  - B0tty Mac B0tface
  - Einstein
  - Ultralila
  - Division3Zehen
** Verwandte:
- [[https://codeberg.org/almamusic/pages][Hier]] bastele ich an der [[https://alma-music.de/][Website meiner Mutter]] und der [[https://cutandsoul.com/][Website von Cut & Soul]].
- Auch mein Vater spielt in einer Band, diese ist allerdings nur auf [[https://www.facebook.com/Skiffle.Skeletons/][Facebook]] zu finden.
- Mein Bruder Elias fertigt Schmuckstücke welche man auf [[https://www.instagram.com/warped_wire/][seinem Instagram Profil]] bewundern kann.
- Mein Bruder Daniel ist inzwischen auch zum Linux nutzer mutiert und [[https://codeberg.org/Tantalus][benutzt sporadisch git]].
- Mein Cousin Christoph, hat schon deutlich länger [[https://chrz.de/][seinen eigenen Blog]] und natürlich auch [[https://github.com/StarGate01][git]].
- Mit [[https://honal.de][Reiner Honal von honal.de]] bin ich zwar entfernt verwandt aber nicht näher bekannt,
- genaueres weis mein Opa Werner Honal. Der (besitzt [[https://honal.eu][honal.eu]] aber) benutzt vorallem [[https://www.w-honal.de/][w-honal.de]] und [[https://honals.de][honals.de]].

* Index librorum commendatorum
** Nette Apps:
- [[https://plantnet.org/en/][PlantNet]] \\
  (Offline) Bild oder genauer Pflanzenerkennung.
- [[https://gitlab.com/oeffi/oeffi][Öffi]] \\
  Eine simple und sehr fähige Navigationsapp für die öffentlichen Transportnetze .
- [[https://github.com/osmandapp/Osmand][OsmAnd~]] \\
  Eine (offline) Navigationsapp mit Karten für hier und dort und überall.
- [[https://fdroid.org][Fdroid]] \\
  Dieser Store hat so ziemlich alles was man an Apps braucht.
- [[https://termux.dev][Termux]] \\
  Ein brauchbares Terminal für Android. \\
  Mit einem Paketmanager der das Herz mit nützlicher Software erheitert.
- [[https://sourceforge.net/projects/android-ports-for-gnu-emacs/][Emacs-Android-Port]] \\
  Besonders genial mit Termuxpaketen.
- [[https://phyphox.org/de/home-de/][Phyphox]] \\
  Dein Smartphone hat Sensoren, nutze sie klug!
  
** Kurioses
|--------------------+------------------------------------------------------------------------|
| Archive            | [[https://annas-archive.org][annas-archive]] [[https://archive.org/][internet-archive]] https://archive.ph/ https://the-eye.eu/ |
| Aromastoffhändler  | [[https://pellwall.com/][PellWall]] [[https://hekserij.nl/][Hekserij]]                                                      |
| Aromastoffinfos    | [[https://bojensen.net/EssentialOilsEng/EssentialOils.htm][Bojensen]] [[http://thegoodscentscompany.com/][thegoodscentscompany]] [[http://www.leffingwell.com][Leffingwell]] [[https://duftstoffverband.de][Duftstoffverband]]             |
| Cybersec           | [[https://amiunique.org][amiunique]] [[https://ccc.de][ccc]] [[https://datadetoxkit.org/][Datadetoxkit]] [[https://datenschmutz.net/][Datenschmutz]]                                |
| Distroinfo         | https://gs.statcounter.com/ [[https://repology.org][Repology]] [[https://distrowatch.org][Distrowatch]]                       |
| Elektronikbauteile | [[https://pollin.de][Pollin]] [[https://www.oppermann-electronic.de/][Oppermann]] [[https://octopart.com/][Octopart]]                                              |
| FOSS               | [[https://fsf.org][FSF]] [[https://gnu.org][Gnu]] [[https://techrights.org/][techrights]] [[https://lwn.net/op/FAQ.lwn][LWN]]                                                 |
| Karten             | [[https://www.openstreetmap.org][OpenStreetMap]] [[https://www.opencyclemap.org][OpenCycleMap]]                                             |
| Kosmetikrohstoffe  | [[https://www.dragonspice.de/][Dragonspice]] [[https://www.jean-puetz-produkte.de/][Jean Pütz]] [[https://www.spinnrad.ch][Spinnrad]]                                         |
| Mail               | https://mxtoolbox.com https://learndmarc.com                           |
| Netzwerk           | [[https://pagekite.net/][PageKite]] [[https://dynv6.com/][DynV6]] [[https://dnshome.org][dnsHome]] https://he.net/                                 |
| Open Hardware      | https://oho.wiki https://opensourceecology.de/                         |
| Revisionskontrolle | [[https://gitlab.com][gitlab]] [[https://sourcehut.org][sourcehut]] [[https://codeberg.org][codeberg]]                                              |
| Spezialchemikalien | [[https://shop.es-drei.de/][S3]] [[http://www.kremer-pigmente.de][Kremer-Pigmente]]                                                     |
| Sprache            | [[https://atlas-alltagssprache.de][atlas-alltagssprache]]                                                   |
| Torrents           | [[https://thepiratebay.org][Piratebay]] [[https://torrents-csv.ml/][Torrents.csv]] [[https://filelisting.com/][Filelisting]] [[https://solidtorrents.net/][Solidtorrents]]                       |
| Toxikologie        | [[http://giftpflanzen.com/][Giftpflanzen]] [[http://www.gifte.de/][Gifte.de]]                                                  |
| Wahlen             | [[https://www.wahl-o-mat.de][Wahl-O-Mat]] [[https://www.wahlrecht.de/umfragen/index.htm][Politbarometer]]                                              |
| Web UI             | [[https://caniuse.com/][caniuse]] [[https://www.w3.org/][w3]] [[https://m3.material.io][m3]] [[https://jsfiddle.net][jsfiddle]] [[https://www.w3schools.com/][w3s]]                                             |
| Whois              | [[https://www.whois.com][whois.com]] [[https://www.denic.de][denic.de]] [[https://www.nic.at][nic.at]] [[https://www.swizzonic.ch/whois][swizzonic.ch]]                                 |
|--------------------+------------------------------------------------------------------------|

** Chemie Lernen
[[http://www.chemie-master.de][Chemie-Master.de]] \\
[[http://www.chemienet.info][Prof. Stuhlpfarrers Chemie-Seiten]]  \\
[[http://www.chemieunterricht.de][Prof. Blumes Bildungsserver für Chemie]]  \\
[[http://www.lebensnaherchemieunterricht.de][Lebensnaherchemieunterricht]]  \\
[[http://www.schuelerlexikon.de][Schuelerlexikon]]  \\
[[http://www.zum.de/Faecher/Ch/SELECTC.HTM][Zentrale für Unterrichtsmedien]] \\
[[https://seilnacht.com/][Seilnacht]] \\
http://chemie.uni-jena.de/institute/oc/weiss/start.html \\
[[https://internetchemie.info/][Internetchemie]] \\
[[https://stevenabbott.co.uk/][Steven Abbott]] \\
[[http://www.chemiestudent.de][Chemiestudent.de]] \\
[[http://www.chymiatrie.de][Chymiatrie]] \\
[[http://www.experimente.net][Experimente.net]] \\
[[https://orgsyn.org][OrgSyn]] \\
[[http://www.experimentalchemie.de][Experimentalchemie]] \\
[[file:///home/macfag/webserver/blog/www.seilnacht.com/Chemie/ch_index.htm][Chemikaliendatenbank]] \\
https://gestis.dguv.de/

** Grenzdebile Foren
Hier gibt es viele sonst schwer zu findende Informationen aber auch einiges an illegaler Sch****. \\
Für die fachlich interessierte Leserschaft prima, zur Nachahmung hingegen mehr als suboptimal. \\
Hier (mit guter Absicht) keine Links:
- +Xplosives.net+ (War ein Forum zu Pyrotechnik, kennt da wer nen Nachfolger?)
- Sciencemadness
- Thevespiary
- Hive
- Rhodium
- Isomerdesign
- Shroomery
- Mycotopia
- Nexus
- Erowid
- Majesticgardens
- Growery
- Future4200
- Fuckcombustion
- Bluelight
- Niggasinspace

