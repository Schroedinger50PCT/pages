let cachename = 'gpoints-v1';

let urls = [
    'modus-vivendi.css',
    'gpoints.html',
    'gpoints.js',
    'division3zehen_white-120.png',
    'division3zehen_white-152.png',
    'division3zehen_white-167.png',
    'division3zehen_white-192.png',
    'division3zehen_white-512.png',
    'division3zehen_white-512-maskable.png',
    'media/fonts/IosevkaTerm-Bold.woff2',
    'media/fonts/IosevkaTerm-Italic.woff2',
    'media/fonts/IosevkaTerm-Regular.woff2'
]   

// Start the service and cache urls.

self.addEventListener('install', event => {
    event.waitUntil(
	caches.open(cachename).then(
	    cache => {
		return cache.addAll(urls);}));});


// offline

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
	caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)}))});
